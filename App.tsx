import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import Dice from './src/components/Dice/Dice';
import changeNavigationBarColor from 'react-native-navigation-bar-color';

function App(): JSX.Element {
  const [value, setValue] = useState(1);

  useEffect(() => {
    changeNavigationBarColor('#000000');
  }, []);

  return (
    <SafeAreaView style={styles.appWrapper}>
      <Dice value={value} />
      <View style={styles.bottomWrapper}>
        <TouchableOpacity
          onPress={() => {
            setValue(Math.round(Math.random() * 5 + 1));
          }}
          style={styles.button}>
          <Text style={styles.text}>Roll</Text>
        </TouchableOpacity>
        <View style={styles.rollTextWrapper}>
          <Text style={styles.text}>{`Roll is ${value}`}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default App;

const styles = StyleSheet.create({
  appWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  button: {
    width: 200,
    height: 44,
    backgroundColor: 'purple',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginTop: 50,
  },
  text: {
    color: 'white',
  },
  rollTextWrapper: {
    marginTop: 24,
  },
  bottomWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
