import {StyleSheet, View} from 'react-native';
import React, {useEffect, useRef} from 'react';
import Animated, {
  Easing,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import DiceSide from '../DiceSide/DiceSide';

const COUNT_ROLL_ANIMATION = 10;
const ANIMATION_CONFIG = {
  duration: 200,
  easing: Easing.circle,
};

const Dice = (props: {value: number}) => {
  const rollValue = useSharedValue(1);
  const rollsValueForAnimation = useRef<number[]>([]);
  const isFirstRender = useRef<boolean>(true);

  useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false;
      return;
    }

    generateRollsValueForAnimation();

    rollAnimation();
  }, [props.value]);

  const generateRollsValueForAnimation = () => {
    // Создаем массив значений для анимации
    rollsValueForAnimation.current = Array.from({
      length: COUNT_ROLL_ANIMATION,
    }).map((_, index) => {
      // Последние значение === значению из вне
      if (index === COUNT_ROLL_ANIMATION - 1) {
        return props.value;
      }
      return Math.round(Math.random() * 5 + 1);
    });

    // чистим от одинаковых подряд цифр
    for (let i = 0; i < rollsValueForAnimation.current.length; i++) {
      for (let j = 0; j < rollsValueForAnimation.current.length; j++) {
        if (
          rollsValueForAnimation.current[j] ===
          rollsValueForAnimation.current[j + 1]
        ) {
          let newNumber = Math.round(Math.random() * 5 + 1);
          while (
            newNumber === rollsValueForAnimation.current[j + 1] ||
            newNumber === rollsValueForAnimation.current[j - 1]
          ) {
            newNumber = Math.round(Math.random() * 5 + 1);
          }
          rollsValueForAnimation.current[j] = newNumber;
        }
      }
    }
  };

  const rollAnimation = () => {
    let index = 0;
    const interval = setInterval(() => {
      if (index === rollsValueForAnimation.current.length - 1) {
        clearInterval(interval);
      }
      const value = rollsValueForAnimation.current[index];
      console.log('value', value);
      rollValue.value = value;
      index++;
    }, 200);
  };

  const side1AnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: withTiming(rollValue.value === 1 ? 1 : 0, ANIMATION_CONFIG),
    };
  });
  const side2AnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: withTiming(rollValue.value === 2 ? 1 : 0, ANIMATION_CONFIG),
    };
  });
  const side3AnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: withTiming(rollValue.value === 3 ? 1 : 0, ANIMATION_CONFIG),
    };
  });
  const side4AnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: withTiming(rollValue.value === 4 ? 1 : 0, ANIMATION_CONFIG),
    };
  });
  const side5AnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: withTiming(rollValue.value === 5 ? 1 : 0, ANIMATION_CONFIG),
    };
  });
  const side6AnimatedStyle = useAnimatedStyle(() => {
    return {
      opacity: withTiming(rollValue.value === 6 ? 1 : 0, ANIMATION_CONFIG),
    };
  });

  return (
    <View style={styles.diceWrapper}>
      <Animated.View style={[styles.diceSide, side1AnimatedStyle]}>
        <DiceSide countDots={1} />
      </Animated.View>
      <Animated.View style={[styles.diceSide, side2AnimatedStyle]}>
        <DiceSide countDots={2} />
      </Animated.View>
      <Animated.View style={[styles.diceSide, side3AnimatedStyle]}>
        <DiceSide countDots={3} />
      </Animated.View>
      <Animated.View style={[styles.diceSide, side4AnimatedStyle]}>
        <DiceSide countDots={4} />
      </Animated.View>
      <Animated.View style={[styles.diceSide, side5AnimatedStyle]}>
        <DiceSide countDots={5} />
      </Animated.View>
      <Animated.View style={[styles.diceSide, side6AnimatedStyle]}>
        <DiceSide countDots={6} />
      </Animated.View>
    </View>
  );
};

export default Dice;

const styles = StyleSheet.create({
  diceWrapper: {
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 12,
  },
  diceSide: {
    backgroundColor: 'white',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    borderRadius: 12,
  },
});
