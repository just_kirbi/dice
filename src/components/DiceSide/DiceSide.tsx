import React, {memo} from 'react';
import {View, StyleSheet} from 'react-native';

const TEMPLATE_DOT = [
  [0, 0, 0, 0, 1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0, 0, 0, 0, 1],
  [1, 0, 0, 0, 1, 0, 0, 0, 1],
  [1, 0, 1, 0, 0, 0, 1, 0, 1],
  [1, 0, 1, 0, 1, 0, 1, 0, 1],
  [1, 1, 1, 0, 0, 0, 1, 1, 1],
];

interface IDiceSideProps {
  countDots: number;
}
const DiceSide = memo(
  (props: IDiceSideProps) => {
    return (
      <View style={styles.diceSideWrapper}>
        {TEMPLATE_DOT[props.countDots - 1].map((dot, index) => (
          <View
            key={`dice-side-${props.countDots}-${index}`}
            style={styles.dotWrapper}>
            <View style={[styles.dot, {opacity: dot}]} />
          </View>
        ))}
      </View>
    );
  },
  () => true,
);

export default DiceSide;

const styles = StyleSheet.create({
  diceSideWrapper: {
    width: 150,
    height: 150,
    flexDirection: 'column',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'center',
  },
  dotWrapper: {
    width: '33%',
    height: '33%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dot: {
    width: 30,
    height: 30,
    backgroundColor: 'black',
    borderRadius: 15,
  },
});
